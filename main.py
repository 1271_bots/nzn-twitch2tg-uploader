import logging
import pathlib
import time

from app import app_args, main_clips, main_archives
from app.config import Config

if __name__ == '__main__':
    __args = app_args()
    _cfg = Config(__args.config, __args.find_in)
    logging.root.setLevel(logging.INFO)
    _app_root = pathlib.Path(__file__).absolute().parent

    _db_root = pathlib.Path(_cfg.storage.root).joinpath(".db.sqlite")

    if not _db_root.is_file():
        _db_root.parent.mkdir(parents=True, exist_ok=True)
        _db_template = _app_root.joinpath(".db.sqlite").read_bytes()
        _db_root.write_bytes(_db_template)

    main_clips(cfg=_cfg, db=str(_db_root))
    main_archives(cfg=_cfg, db=str(_db_root))

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            logging.warning("Interrupting...")
            time.sleep(.1)
            exit(1)
