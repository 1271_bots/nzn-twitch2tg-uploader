import pathlib
import re
import typing
from pathlib import Path
import inspect

import toml


class _cfg:
    def __init__(self, **kwargs):
        _annotations = inspect.get_annotations(self.__class__)
        for k in _annotations.keys():
            if k not in kwargs.keys():
                raise RuntimeError(f"""Argument "{k}" not found.""")
            if hasattr(self, f"_make_{k}"):
                setattr(self, k, getattr(self, f"_make_{k}")(kwargs[k]))
                continue
            if type(kwargs[k]) is not _annotations[k]:
                raise RuntimeError(f"""Argument "{k}" have wrong type. Expected {type(k)} found {_annotations[k]}""")
            if hasattr(self, f"_check_{k}"):
                getattr(self, f"_check_{k}")(kwargs[k])
            setattr(self, k, kwargs[k])

    @staticmethod
    def _check_check_time(k):
        if k < 1:
            raise RuntimeError("check_time must be greater than zero.")


class ConfigTwitch(_cfg):
    channel: str
    filter: str
    range: str
    check_time: int

    @staticmethod
    def _check_range(k):
        allowed_values = ("24hr", "7d", "30d")
        if k not in allowed_values:
            raise RuntimeError("Bad range value {}. Expected one of {}".format(k, "/".join(allowed_values)))


class ConfigDelete(_cfg):
    enabled: bool
    days: int


class ConfigArchive(_cfg):
    enabled: bool
    root: str
    days: int
    delete: ConfigDelete

    @classmethod
    def _make_delete(cls, val: dict) -> ConfigDelete:
        return ConfigDelete(**val)


class ConfigStorage(_cfg):
    root: str
    check_time: int
    archive: ConfigArchive

    @classmethod
    def _make_archive(cls, val: dict) -> ConfigArchive:
        return ConfigArchive(**val)


class ConfigTelegram(_cfg):
    token: str
    channel_id: str


class ConfigUtils(_cfg):
    ytdl: str
    ffmpeg: str

    @staticmethod
    def _check_ytdl(k):
        if not pathlib.Path(k).is_file():
            raise RuntimeError(f"{k} is not file")

    @staticmethod
    def _check_ffmpeg(k):
        if not pathlib.Path(k).is_file():
            raise RuntimeError(f"{k} is not file")


class Config:
    twitch: ConfigTwitch
    storage: ConfigStorage
    telegram: ConfigTelegram
    utils: ConfigUtils

    def __init__(self, config: str, find_in: typing.List[str]):
        path = self.__cfg(config, find_in)
        if not path.is_file():
            raise RuntimeError(f"Configuration file not found ({path})")
        with path.open() as r:
            __data = toml.load(r)
            self.twitch = ConfigTwitch(**__data.get("twitch", {}))
            self.storage = ConfigStorage(**__data.get("storage", {}))
            self.telegram = ConfigTelegram(**__data.get("telegram", {}))
            self.utils = ConfigUtils(**__data.get("utils", {}))

    @classmethod
    def __cfg(cls, config: str, find_in: typing.List[str]) -> Path:
        if not re.match(r"[a-zA-Z0-9_-]+\.toml$", config):
            raise RuntimeError("The configuration path must end with .toml")
        path = Path(config)
        if path.is_file():
            return path
        for i in find_in:  # type: str
            if i == ".":
                path = Path(__file__).absolute().parent.parent
            elif i.startswith("/"):
                path = Path(i).absolute()
            else:
                path = Path(__file__).absolute().parent.parent.joinpath(i)
            path = path.joinpath(config)
            if path.is_file():
                return path
