import argparse

from app._main_archives import main_archives
from app._main_clips import main_clips


def app_args() -> argparse.Namespace:
    args = argparse.ArgumentParser()

    args.add_argument("--config", "-c", nargs=1, default="config.toml")
    args.add_argument("--find_in", nargs="+", default=["/etc/clips_uploader/", "."])

    return args.parse_args()
