import threading
import typing

from app.config import Config


class _main_archives:
    def __init__(self, cfg: Config):
        self.cfg = cfg

    def run(self):
        return []


def main_archives(cfg: Config, db: str) -> typing.List[threading.Thread]:
    _app = _main_archives(cfg)
    return _app.run()

