import json
import logging
import pathlib
import re
import subprocess
import threading
import time
import typing
from datetime import datetime, timezone, timedelta
from PIL import Image

import httpx
import telebot

from app._sql import ClipsTable
from app.config import Config

TZ = timezone(timedelta(hours=3))
RE = re.compile(r".+\.mp4$")


class _main_clips:
    # """
    # https://github.com/yt-dlp/yt-dlp#output-template-examples
    # "%(upload_date>%Y)s/%(title)s.%(ext)s"
    # """
    _filename_fmt = r"%(id.0:2)s/%(id.2:4)s/%(id)s-%(display_id)s.%(ext)s"
    _clip_time_fmt = r"%Y-%m-%d %H:%M:%S"
    _lock = []
    _converted = []

    def __init__(self, cfg: Config, db: str):
        self.cfg = cfg
        self.db = ClipsTable(db)
        self.bot = telebot.TeleBot(
            cfg.telegram.token
        )

    def run(self):
        thread_download = threading.Thread(target=self._download)
        thread_download.start()
        logging.info("Download thread started")

        thread_upload = threading.Thread(target=self._upload)
        thread_upload.start()
        logging.info("Upload thread started")

        thread_converter = threading.Thread(target=self._converter)
        thread_converter.start()
        logging.info("Converter thread started")

        return [
            thread_download,
            thread_upload,
            thread_converter,
        ]

    def _download(self):
        while True:
            time.sleep(1)
            logging.info("Check new clips: start")
            result = subprocess.call([
                self.cfg.utils.ytdl,
                "-P",
                self.cfg.storage.root,
                "-o",
                self._filename_fmt,
                "--write-info-json",
                r"https://www.twitch.tv/{channel}/{filter}?filter={filter}&range={range}".format(
                    channel=self.cfg.twitch.channel,
                    filter=self.cfg.twitch.filter,
                    range=self.cfg.twitch.range,
                )
            ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            if result == 0:
                logging.info("Check new clips: ok")
            else:
                logging.warning(f"Check new clips: error with status {result}")

            time.sleep(60 * self.cfg.twitch.check_time)

    def _upload(self):
        root = pathlib.Path(self.cfg.storage.root)
        while True:
            time.sleep(1)
            logging.info("Upload loop")

            fs_app = pathlib.Path(__file__).absolute().parent.parent.joinpath("var", "fs_struct_to_db")

            result = subprocess.run([
                str(fs_app),
                "-sqr",
                f"{root}",
            ], capture_output=True)
            if result.returncode == 0:
                files = json.loads(result.stdout)['files']
            else:
                logging.warning(f"Bad fs app code: {result.returncode}")
                files = []

            for f in files:
                if not RE.match(f):
                    logging.debug(f"Skip {f} != mp4")
                    continue
                file = root.joinpath(f).resolve()
                if not self.__meta(file).is_file():
                    logging.info(f"Skip {f} != meta")
                    continue
                if self.db.clip_exists(file):
                    logging.debug(f"Clip {f} exists.")
                    continue
                if file.name in self._lock:
                    logging.debug(f"Skip {f} locked")
                    continue
                if file.stat().st_size >= 49_000_000:
                    logging.info(f"File {f} too big. Skip.")
                    self._lock.append(f)
                    continue
                self.__uploader(file)
                time.sleep(10)
            time.sleep(60 * self.cfg.storage.check_time)

    def __uploader(self, file: pathlib.Path):
        logging.info(f"Upload clip {file.name}")

        img = self.__img(file)
        meta = json.loads(self.__meta(file).read_text())

        if not img.is_file():
            n = 5
            ok = False
            while n > 0:
                n -= 1

                tmb_url = self.__tmb(file)
                if tmb_url is None:
                    img.write_bytes(
                        pathlib.Path(__file__).absolute().parent.parent.joinpath("tmb.jpg").read_bytes()
                    )
                    break

                tmb = httpx.get(tmb_url, follow_redirects=True)
                ok = tmb.status_code < 400
                logging.info(f"Status {tmb.status_code} for {img}")
                if ok:
                    img.write_bytes(tmb.content)
                tmb.close()
                if ok:
                    break
                time.sleep(3)
            if not ok:
                logging.warning(f"Cannot get thumbnail for {img.name}")
                return
        pil_img = Image.open(img)
        if pil_img.width > 320:
            new_img = pil_img.resize((
                320,
                int(pil_img.height * (320 / pil_img.width))
            ))
            pil_img.close()
            new_img.save(img)

        dt = datetime.fromtimestamp(
            float(meta["timestamp"]),
            TZ,
        ).strftime(self._clip_time_fmt)

        logging.info(f"Uploading file {file.name} to telegram")

        try:
            self.bot.send_video(
                chat_id=self.cfg.telegram.channel_id,
                video=telebot.types.InputFile(file),
                thumbnail=telebot.types.InputFile(img),
                supports_streaming=True,
                disable_notification=True,
            )

            link = "https://www.twitch.tv/{}/{}/{}".format(
                self.cfg.twitch.channel,
                self.cfg.twitch.filter,
                meta["display_id"],
            )
            text = (
                'New clip: <code>{}</code>'
                '\nDate: <code>{}</code>'
                '\nId: <code>{}</code>'
                '\nAuthor: <code>{}</code>'
                '\nUrl: <a href="{}">{}</a>'
            ).format(
                meta["title"].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"),
                dt,
                meta["id"],
                meta["uploader"],
                link,
                meta["display_id"],
            )

            self.bot.send_message(
                chat_id=self.cfg.telegram.channel_id,
                text=text,
                parse_mode="HTML",
                disable_notification=True,
            )
        except (telebot.apihelper.ApiTelegramException, TimeoutError, Exception) as e:
            logging.error(f"{e}")
            return

        logging.info(f"Add file to db: {file.name}")

        self.db.add_clip(file, self.__meta(file))

    @classmethod
    def __meta(cls, file: pathlib.Path) -> pathlib.Path:
        return file.with_suffix(".info.json")

    @classmethod
    def __img(cls, file: pathlib.Path) -> pathlib.Path:
        return file.with_suffix(".jpg")

    @classmethod
    def __tmb(cls, file: pathlib.Path) -> typing.Optional[str]:
        data = json.loads(cls.__meta(file).read_text())
        try:
            item = max(data["thumbnails"], key=lambda x: x["width"])
            return item["url"]
        except (IndexError, KeyError):
            for item in data["thumbnails"]:
                if item["id"] == "medium":
                    return item["url"]
        return None

    def _converter(self):
        while True:
            if len(self._lock) == 0:
                time.sleep(.1)
                continue
            _files = []
            _do = [f for f in self._lock]
            for f in _do:
                src = pathlib.Path(f)
                dst = src.parent.with_name(f"_{src.name}")
                logging.info(f"Convert {src.name}")
                code = subprocess.call([
                    self.cfg.utils.ffmpeg,
                    "-i",
                    f"{src}",
                    "-c:v",
                    "h264",
                    "-c:a",
                    "copy",
                    "-b:v",
                    "2500K",
                    f"{dst}",
                ], stdout=subprocess.DEVNULL)
                if code != 0:
                    logging.warning(f"Bad ffmpeg code: {code} for {src.name}")
                    continue

                _bkp = pathlib.Path(self.cfg.storage.root).joinpath(
                    ".backup",
                    src.name[:2],
                    src.name[2:4],
                )
                _bkp.mkdir(parents=True, exist_ok=True)

                src.rename(_bkp.joinpath(src.name))
                dst.rename(f)

                _files.append(f)

            for f in _files:
                self._lock.remove(f)


def main_clips(cfg: Config, db: str) -> typing.List[threading.Thread]:
    _app = _main_clips(cfg, db)
    return _app.run()
