import json
import pathlib
import re
import sqlite3

RE_ID = re.compile(r"^(\d+)-")


class ClipsTable:
    def __init__(self, db: str):
        self.db = db

    def add_clip(self, path: pathlib.Path, meta: pathlib.Path):
        _db = sqlite3.Connection(self.db)
        cur = _db.cursor()
        name = path.name
        clip_id = RE_ID.search(name)
        clip_id = "-1" if clip_id is None else clip_id.group(1)

        meta = json.loads(meta.read_text())
        dt = int(meta["timestamp"])

        cur.execute(
            """INSERT INTO clips (id, clip_name, dt) VALUES (?, ?, ?)""",
            (clip_id, name, dt),
        )

        cur.close()
        _db.commit()
        _db.close()

    def clip_exists(self, path: pathlib.Path):
        _db = sqlite3.Connection(self.db)
        cur = _db.cursor()
        name = path.name
        result = cur.execute(
            """
            SELECT * FROM clips WHERE clip_name = ?
            """,
            (name, )
        ).fetchone()
        exists = result is not None
        cur.close()
        _db.close()
        return exists
